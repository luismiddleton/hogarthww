# Luis Middleton - Hogarth WW

## Table of Contents

- [1. Setup](#setup)
  - [Clone project](#clone-project)
  - [Install Dependencies](#install-dependencies)
  - [Run HTTP server](#run-http-server)
- [2. NodeJS](#nodejs)
  - [Dependencies](#dependencies)
  - [Dev Dependencies](#dev-dependencies)
- [3. SASS](#sass)
  - [Lint specifications](#lint-specifications)
  - [Build folder tree](#build-folder-tree)
  - [Linting SASS files](#linting-sass-files)
  - [Compile SASS files](#compile-sass-files)
  - [Run HTTP server in development mode](#run-http-server-in-development-mode)

---

## Setup

### Clone project

```
sudo git clone https://luismiddleton@bitbucket.org/luismiddleton/hogarthww.git
```

### Install Dependencies

First, let's switch to folder

```
cd hogarthww
```

and install packages

```
sudo npm i
```

### Run HTTP server

Once packages have been installed, we will start a HTTP server using [@zeit/serve](https://github.com/zeit/serve)

```
sudo npm start
```

Once the command has been sucessfully executed, our `index.html` will be accessible from [http://localhost:5000](http://localhost:5000)

_Note_ - This will also change NODE_ENV to `production`

---

## NodeJS

#### Dependencies

- "node-sass": "^4.9.3",
- "serve": "^10.0.0"

#### Dev Dependencies

- "autoprefixer": "^9.1.3",
- "gulp": "^3.9.1",
- "gulp-cli": "^2.0.1",
- "gulp-postcss": "^8.0.0",
- "gulp-sass": "^4.0.1",
- "gulp-sass-lint": "^1.4.0",
- "gulp-sourcemaps": "^2.6.4",
- "precss": "^3.1.2",
- "sass-lint": "^1.12.1"

---

## Sass

#### Lint specifications

- Using [Airbnb's styleguide](https://github.com/airbnb/css) rules.
- Config file can be found at `.sasslint.yml`

#### Build folder tree structure

```
build
└── sass
    ├── components
    │   └── _components.scss
    ├── mixins
    │   └── _mixins.scss
    ├── mod.scss
    └── variables
        └── _variables.scss
```

`mod.scss` will be the index file for our SASS build.

#### Linting SASS files

Run the following command:

```
sudo npm run sass:lint
```

The gulp task will lint through all `.scss` files and output any warnings/errors to the console. Linting rules are specified within the `.sasslint.yml` config file

#### Compile SASS files

Run the following command:

```
sudo npm run sass:build
```

The gulp task will output a compressed version of `mod.css` to the following path: `/css/mod.css`

_Note_ - The task will also run should `npm start` be executed from command line.

#### Run HTTP server in development mode

Run the following command:

```
sudo npm run sass:dev
```

The gulp task will output a linted and syntatically expanded version of `mod.css` and run `http://localhost:5000` in development mode.
