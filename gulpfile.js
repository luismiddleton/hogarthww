var gulp = require("gulp");
var sass = require("gulp-sass");
var sourcemaps = require("gulp-sourcemaps");
var postcss = require("gulp-postcss");
var sassLint = require("gulp-sass-lint");
var autoprefixer = require("autoprefixer");
var precss = require("precss");
var input = "./build/sass/**/*.scss";
var sassConfig = "./.sasslint.yml";
var plugins = [autoprefixer(), precss()];

gulp.task("sass:lint", function() {
  return gulp
    .src(input)
    .pipe(
      sassLint({
        options: {
          configFile: sassConfig
        }
      })
    )
    .pipe(sassLint.format())
    .pipe(sassLint.failOnError());
});

gulp.task("sass:dev", function() {
  return gulp
    .src(input)
    .pipe(sourcemaps.init())
    .pipe(sass({ outputStyle: "expanded" }).on("error", sass.logError))
    .pipe(postcss(plugins))
    .pipe(sourcemaps.write("."))
    .pipe(gulp.dest("./css/"));
});

gulp.task("sass:build", function() {
  return gulp
    .src(input)
    .pipe(sourcemaps.init())
    .pipe(sass({ outputStyle: "compressed" }).on("error", sass.logError))
    .pipe(postcss(plugins))
    .pipe(sourcemaps.write("."))
    .pipe(gulp.dest("./css/"));
});

gulp.task("dev", ["sass:lint", "sass:dev"]);
gulp.task("default", ["sass:lint", "sass:build"]);
